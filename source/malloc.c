#include <stdio.h>
#include <stdlib.h>

struct mab
{
  int offset;
  int size;
  int allocated;
  struct mab *next;
  struct mab *prev;
};
typedef struct mab *MabPtr;
typedef MabPtr Memory;
typedef struct mab Mab;

Memory
create (int size);
void
allocate (Memory memory);
void
first_fit (Memory memory, int proc, int size);
void
best_fit (Memory memory, int proc, int size);
void
worst_fit (Memory memory, int proc, int size);
void
deallocate (Memory memory);
void
display (Memory memory);
void
clean (Memory memory);

int
main ()
{
  int opt;
  Memory memory;

  memory = create (10000);
  for (;;)
    {
      printf ("\n---------MENU---------");
      printf ("\n1.Allocar Memoria");
      printf ("\n2.Desallocar Memoria");
      printf ("\n3.Mostrar");
      printf ("\n4.Salir");
      printf ("\n----------------------");
      printf ("\nEjecutar: ");
      scanf (" %d", &opt);
      switch (opt)
	{
	case 1:
	  allocate (memory);
	  break;
	case 2:
	  deallocate (memory);
	  break;
	case 3:
	  display (memory);
	  break;
	case 4:
	  clean (memory);
	  exit (0);
	}
    }
}

Memory
create (int size)
{
  Memory memory;
  memory = (Memory) malloc (sizeof(Mab));
  memory->size = size;
  memory->allocated = 0;
  memory->offset = 0;
  memory->prev = NULL;
  memory->next = NULL;
  return memory;
}

void
allocate (Memory memory)
{
  int opt, id, size;
  printf ("\nID del Proceso: ");
  scanf (" %d", &id);
  printf ("\nTamano del proceso: ");
  scanf (" %d", &size);
  printf ("\nMetodo:");
  printf ("\n1.First fit");
  printf ("\n2.Best fit");
  printf ("\n3.Worst fit");
  printf ("\nUtilizar: ");
  scanf (" %d", &opt);
  switch (opt)
    {
    case 1:
      first_fit (memory, id, size);
      break;
    case 2:
      best_fit (memory, id, size);
      break;
    case 3:
      worst_fit (memory, id, size);
      break;
    }
}

void
first_fit (Memory memory, int proc, int size)
{
  int diff;
  Memory tmp;
  tmp = memory;
  while (tmp != NULL)
    {
      if (tmp->size >= size && tmp->allocated == 0)
	{
	  tmp->allocated = proc;
	  diff = tmp->size - size;
	  tmp->size = size;
	  if (tmp->next != NULL && tmp->next->allocated == 0)
	    {
	      tmp = tmp->next;
	      tmp->size += diff;
	      tmp->offset -= diff;
	    }
	  else if (tmp->next != NULL && tmp->next->allocated != 0)
	    {
	      Memory t;
	      t = (Memory) malloc (sizeof(Mab));
	      t->allocated = 0;
	      t->size = diff;
	      t->offset = tmp->offset + tmp->size;
	      t->prev = tmp;
	      t->next = tmp->next;
	      tmp->next = t;
	      tmp->next->prev = t;
	    }
	  else
	    {
	      Memory t;
	      t = (Memory) malloc (sizeof(Mab));
	      t->allocated = 0;
	      t->size = diff;
	      t->offset = tmp->offset + tmp->size;
	      t->prev = tmp;
	      t->next = NULL;
	      tmp->next = t;
	    }
	  break;
	}
      else
	tmp = tmp->next;
    }
}

void
best_fit (Memory memory, int proc, int size)
{
  int diff, count, pos, min;
  Memory tmp;
  tmp = memory;
  min = 10001;
  count = -1;
  while (tmp != NULL)
    {
      count++;
      if (tmp->allocated == 0)
	{
	  if (tmp->size >= size && tmp->size < min)
	    {
	      min = tmp->size;
	      pos = count;
	    }
	}
      tmp = tmp->next;
    }

  tmp = memory;
  count = -1;
  while (tmp != NULL)
    {
      count++;
      if (count == pos)
	{
	  tmp->allocated = proc;
	  diff = tmp->size - size;
	  tmp->size = size;
	  if (diff > 0)
	    {
	      if (tmp->next != NULL && tmp->next->allocated == 0)
		{
		  tmp = tmp->next;
		  tmp->size += diff;
		  tmp->offset -= diff;
		}
	      else if (tmp->next != NULL && tmp->next->allocated != 0)
		{
		  Memory t;
		  t = (Memory) malloc (sizeof(Mab));
		  t->allocated = 0;
		  t->size = diff;
		  t->offset = tmp->offset + tmp->size;
		  t->prev = tmp;
		  t->next = tmp->next;
		  tmp->next = t;
		  tmp->next->prev = t;
		}
	      else
		{
		  Memory t;
		  t = (Memory) malloc (sizeof(Mab));
		  t->allocated = 0;
		  t->size = diff;
		  t->offset = tmp->offset + tmp->size;
		  t->prev = tmp;
		  t->next = NULL;
		  tmp->next = t;
		}
	    }
	}
      tmp = tmp->next;
    }
}

void
worst_fit (Memory memory, int proc, int size)
{
  int diff, count, pos, max;
  Memory tmp;
  tmp = memory;
  max = -1;
  count = -1;
  while (tmp != NULL)
    {
      count++;
      if (tmp->size >= size && tmp->allocated == 0)
	if (tmp->size > max)
	  {
	    max = tmp->size;
	    pos = count;
	  }
      tmp = tmp->next;
    }

  tmp = memory;
  count = -1;
  while (tmp != NULL)
    {
      count++;
      if (count == pos)
	{
	  tmp->allocated = proc;
	  diff = tmp->size - size;
	  tmp->size = size;
	  if (diff > 0)
	    {
	      if (tmp->next != NULL && tmp->next->allocated == 0)
		{
		  tmp = tmp->next;
		  tmp->size += diff;
		  tmp->offset -= diff;
		}
	      else if (tmp->next != NULL && tmp->next->allocated != 0)
		{
		  Memory t;
		  t = (Memory) malloc (sizeof(Mab));
		  t->allocated = 0;
		  t->size = diff;
		  t->offset = tmp->offset + tmp->size;
		  t->prev = tmp;
		  t->next = tmp->next;
		  tmp->next = t;
		  tmp->next->prev = t;
		}
	      else
		{
		  Memory t;
		  t = (Memory) malloc (sizeof(Mab));
		  t->allocated = 0;
		  t->size = diff;
		  t->offset = tmp->offset + tmp->size;
		  t->prev = tmp;
		  t->next = NULL;
		  tmp->next = t;
		}
	    }
	}
      tmp = tmp->next;
    }
}

void
deallocate (Memory memory)
{
  Memory tmp;
  int proc;
  printf ("\n Ingrese el ID del Proceso: ");
  scanf (" %d", &proc);
  tmp = memory;
  while (tmp != NULL)
    {
      if (tmp->allocated == proc)
	{
	  if (tmp->next != NULL && tmp->next->allocated == 0)
	    {
	      tmp->next->offset = tmp->offset;
	      tmp->next->size = tmp->next->size + tmp->size;
	      tmp->next->prev = tmp->prev;
	      if (tmp->prev != NULL)
		{
		  tmp->prev->next = tmp->next;
		}
	      else
		{
		  memory = tmp->next;
		}
	      free (tmp);
	    }
	  else if (tmp->next != NULL && tmp->next->allocated != 0)
	    {
	      tmp->allocated = 0;
	    }
	  break;
	}
      tmp = tmp->next;
    }
  tmp = memory;
  while (tmp != NULL)
    {
      if (tmp->allocated == 0 && tmp->next != NULL && tmp->next->allocated == 0)
	{
	  tmp->next->offset = tmp->offset;
	  tmp->next->size = tmp->next->size + tmp->size;
	  tmp->next->prev = tmp->prev;
	  tmp->prev->next = tmp->next;
	  if (tmp->prev == NULL)
	    {
	      memory = tmp->next;
	    }
	  free (tmp);
	  break;
	}
      tmp = tmp->next;
    }
}

void
display (Memory memory)
{
  Memory tmp;
  tmp = memory;
  while (tmp != NULL)
    {
      if (tmp->allocated != 0)
	{
	  printf ("\n-Proceso %i (size: %i - start: %i - end: %i)",
		  tmp->allocated, tmp->size, tmp->offset,
		  tmp->size + tmp->offset);
	}
      else
	{
	  printf ("\n-Espacio Libre (size: %i - start: %i - end: %i)",
		  tmp->size, tmp->offset, tmp->size + tmp->offset);
	}
      tmp = tmp->next;
    }
  printf ("\n");
}

void
clean (Memory memory)
{
  while (memory->next != NULL)
    {
      memory = memory->next;
      free (memory->prev);
    }
  free (memory);
}
